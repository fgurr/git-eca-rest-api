SHELL = /bin/bash
setup: clean start-services;
dev-start: setup;
	(CONFIG_SECRET_PATH=$$PWD/config/application/secret.properties mvn compile quarkus:dev -B -Dconfig.secret.properties=$$PWD/config/application/secret.properties -Djava.net.preferIPv4Stack=true)
clean:;
	mvn clean
## Compilation steps
install-yarn:;
	yarn install --frozen-lockfile --audit
compile: compile-java;
compile-quick: compile-java-quick;
compile-java: generate-spec;
	mvn clean verify -B
compile-java-quick: generate-spec;
	mvn clean verify -B -Dmaven.test.skip=true
generate-spec: validate-spec;
	yarn run generate-json-schema
validate-spec: install-yarn;
compile-start: compile-quick start;
## Docker/service binds - services soft split from application to enable dev-start easier
start:;
	docker compose build
	docker compose up -d
start-services: stop-services;
	docker compose up mariadb -d
start-opt: stop-opt;
	docker compose --profile optional-services up -d
stop: stop-application;
stop-application:;
	docker compose down
stop-services:;
	docker compose stop mariadb
stop-opt:;
	docker compose --profile optional-services down
stop-all: stop-application stop-services stop-opt;
## Start njs server w/ openapi spec
start-spec: validate-spec;
	yarn run start
## Custom Testing calls
test-post-git-eca:;
	curl http://api.eclipse.dev.docker:8090/git/eca -v -H 'Content-Type: application/json' -d @config/json/post-git-eca.json
test-dev-post-git-eca:;
	curl http://api.eclipse.dev.docker:8080/git/eca -v -H 'Content-Type: application/json' -d @config/json/post-git-eca.json
gitlab-root-pw-reset:;
	docker exec -it $$(docker compose ps -q gitlab) gitlab-rake "gitlab:password:reset[root]"
