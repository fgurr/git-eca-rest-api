/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.util.Optional;

import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
class ReportsResourceTest {
    public static final String REPORTS_BASE_URL = "/reports";
    public static final String REPORTS_PROJECTS_URL = REPORTS_BASE_URL + "/gitlab/private-projects?key={key}";
    public static final String REPORTS_PROJECTS_STATUS_URL = REPORTS_PROJECTS_URL + "&status={status}";
    public static final String REPORTS_PROJECTS_UNTIL_URL = REPORTS_PROJECTS_URL + "&until={date}";
    public static final String REPORTS_PROJECTS_SINCE_URL = REPORTS_PROJECTS_URL + "&since={date}";
    public static final String REPORTS_PROJECTS_RANGE_URL = REPORTS_PROJECTS_URL + "&since={start}&until={end}";
    public static final String VALID_TEST_ACCESS_KEY = "samplekey";

    public static final EndpointTestCase GET_REPORT_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(REPORTS_PROJECTS_URL, new String[] { VALID_TEST_ACCESS_KEY },
                    SchemaNamespaceHelper.PRIVATE_PROJECT_EVENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_ACTIVE_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(REPORTS_PROJECTS_STATUS_URL,
                    new String[] { VALID_TEST_ACCESS_KEY, GitEcaParameterNames.STATUS_ACTIVE.getName() },
                    SchemaNamespaceHelper.PRIVATE_PROJECT_EVENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_DELETED_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(REPORTS_PROJECTS_STATUS_URL,
                    new String[] { VALID_TEST_ACCESS_KEY, GitEcaParameterNames.STATUS_DELETED.getName() },
                    SchemaNamespaceHelper.PRIVATE_PROJECT_EVENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_SINCE_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(REPORTS_PROJECTS_SINCE_URL, new String[] { VALID_TEST_ACCESS_KEY, "2022-11-11" },
                    SchemaNamespaceHelper.PRIVATE_PROJECT_EVENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_UNTIL_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(REPORTS_PROJECTS_UNTIL_URL, new String[] { VALID_TEST_ACCESS_KEY, "2022-11-15" },
                    SchemaNamespaceHelper.PRIVATE_PROJECT_EVENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_RANGE_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(REPORTS_PROJECTS_RANGE_URL, new String[] { VALID_TEST_ACCESS_KEY, "2022-11-15", "2022-11-15" },
                    SchemaNamespaceHelper.PRIVATE_PROJECT_EVENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_BAD_ACCESS_KEY = EndpointTestCase
            .builder()
            .setPath(REPORTS_PROJECTS_URL)
            .setStatusCode(401)
            .setParams(Optional.of(new String[] { "incorrect-key" }))
            .build();

    public static final EndpointTestCase GET_REPORT_BAD_STATUS_CASE = TestCaseHelper
            .buildBadRequestCase(REPORTS_PROJECTS_STATUS_URL, new String[] { VALID_TEST_ACCESS_KEY, "nope" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_BAD_UNTIL_CASE = TestCaseHelper
            .buildBadRequestCase(REPORTS_PROJECTS_UNTIL_URL, new String[] { VALID_TEST_ACCESS_KEY, "nope" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_REPORT_BAD_SINCE_CASE = TestCaseHelper
            .buildBadRequestCase(REPORTS_PROJECTS_SINCE_URL, new String[] { VALID_TEST_ACCESS_KEY, "nope" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET /reports/gitlab/private-projects
     */
    @Test
    void getPrivProjReport_success() {
        RestAssuredTemplates.testGet(GET_REPORT_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReport_success_validateSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReport_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_REPORT_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReport_failure_invalidRequestFormat() {
        RestAssuredTemplates
                .testGet(TestCaseHelper
                        .buildInvalidFormatCase(REPORTS_PROJECTS_URL, new String[] { VALID_TEST_ACCESS_KEY }, ContentType.TEXT));
    }

    @Test
    void getPrivProjReport_failure_badAccessKey() {
        RestAssuredTemplates.testGet(GET_REPORT_BAD_ACCESS_KEY);
    }

    /*
     * GET /reports/gitlab/private-projects?status=active
     */
    @Test
    void getPrivProjReportActive_success() {
        RestAssuredTemplates.testGet(GET_REPORT_ACTIVE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportActive_success_validateSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_ACTIVE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportActive_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_REPORT_ACTIVE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportStatus_failure_invalidStatus() {
        RestAssuredTemplates.testGet(GET_REPORT_BAD_STATUS_CASE);
    }

    @Test
    void getPrivProjReportStatus_failure_invalidStatus_validate_schema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_BAD_STATUS_CASE);
    }

    /*
     * GET /reports/gitlab/private-projects?status=deleted
     */
    @Test
    void getPrivProjReportDeleted_success() {
        RestAssuredTemplates.testGet(GET_REPORT_DELETED_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportDeleted_success_validateSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_DELETED_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportDeleted_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_REPORT_DELETED_SUCCESS_CASE);
    }

    /*
     * GET /reports/webhooks/gitlab/system?since={date}
     */
    @Test
    void getPrivProjReportSince_success() {
        RestAssuredTemplates.testGet(GET_REPORT_SINCE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportSince_success_validateSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_SINCE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportSince_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_REPORT_SINCE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportSince_failure_invalidStatus() {
        RestAssuredTemplates.testGet(GET_REPORT_BAD_SINCE_CASE);
    }

    @Test
    void getPrivProjReportSince_failure_invalidStatus_validate_schema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_BAD_SINCE_CASE);
    }

    /*
     * GET /reports/gitlab/private-projects?until={date}
     */
    @Test
    void getPrivProjReportUntil_success() {
        RestAssuredTemplates.testGet(GET_REPORT_UNTIL_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportUntil_success_validateSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_UNTIL_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportUntil_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_REPORT_UNTIL_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportUntil_failure_invalidStatus() {
        RestAssuredTemplates.testGet(GET_REPORT_BAD_UNTIL_CASE);
    }

    @Test
    void getPrivProjReportSUntil_failure_invalidStatus_validate_schema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_BAD_UNTIL_CASE);
    }

    /*
     * GET /reports/webhooks/gitlab/system?since={date}&until={date}
     */
    @Test
    void getPrivProjReportRange_success() {
        RestAssuredTemplates.testGet(GET_REPORT_RANGE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportRange_success_validateSchema() {
        RestAssuredTemplates.testGet_validateSchema(GET_REPORT_RANGE_SUCCESS_CASE);
    }

    @Test
    void getPrivProjReportRange_success_validateResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_REPORT_RANGE_SUCCESS_CASE);
    }
}
