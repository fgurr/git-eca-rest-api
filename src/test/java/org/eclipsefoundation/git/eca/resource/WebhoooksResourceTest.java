/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.git.eca.api.models.SystemHook;
import org.eclipsefoundation.git.eca.api.models.SystemHook.Owner;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class WebhoooksResourceTest {
    public static final String WEBHOOKS_BASE_URL = "/webhooks";
    public static final String GITLAB_HOOK_URL = WEBHOOKS_BASE_URL + "/gitlab/system";

    public static final SystemHook PROJECT_CREATE_HOOK_VALID = SystemHook.builder()
            .setCreatedAt(ZonedDateTime.now())
            .setUpdatedAt(ZonedDateTime.now())
            .setEventName("project_create")
            .setName("TestProject")
            .setOwnerEmail("testuser@eclipse-foundation.org")
            .setOwnerName("Test User")
            .setOwners(Arrays.asList(Owner.builder()
                    .setEmail("projectmaker@eclipse-foundation.org")
                    .setName("Project Maker")
                    .build()))
            .setPath("TestProject")
            .setPathWithNamespace("testuser/testproject")
            .setProjectId(69)
            .setProjectVisibility("private")
            .build();

    public static final SystemHook PROJECT_DESTROY_HOOK_VALID = SystemHook.builder()
            .setCreatedAt(ZonedDateTime.now())
            .setUpdatedAt(ZonedDateTime.now())
            .setEventName("project_destroy")
            .setName("TestProject")
            .setOwnerEmail("testuser@eclipse-foundation.org")
            .setOwnerName("Test User")
            .setOwners(Arrays.asList(Owner.builder()
                    .setEmail("projectmaker@eclipse-foundation.org")
                    .setName("Project Maker")
                    .build()))
            .setPath("TestProject")
            .setPathWithNamespace("testuser/testproject")
            .setProjectId(69)
            .setProjectVisibility("private")
            .build();

    public static final SystemHook PROJECT_RENAME_HOOK_VALID = SystemHook.builder()
            .setCreatedAt(ZonedDateTime.now())
            .setUpdatedAt(ZonedDateTime.now())
            .setEventName("project_rename")
            .setName("TestProject")
            .setOwnerEmail("testuser@eclipse-foundation.org")
            .setOwnerName("Test User")
            .setOwners(Arrays.asList(Owner.builder()
                    .setEmail("projectmaker@eclipse-foundation.org")
                    .setName("Project Maker")
                    .build()))
            .setPath("TestProject")
            .setPathWithNamespace("testuser/newname")
            .setProjectId(69)
            .setProjectVisibility("private")
            .setOldPathWithNamespace("testuser/testproject")
            .build();

    public static final SystemHook HOOK_NOT_TRACKED = SystemHook.builder()
            .setCreatedAt(ZonedDateTime.now())
            .setUpdatedAt(ZonedDateTime.now())
            .setEventName("project_update")
            .setName("TestProject")
            .setOwnerEmail("testuser@eclipse-foundation.org")
            .setOwnerName("Test User")
            .setOwners(Arrays.asList(Owner.builder()
                    .setEmail("projectmaker@eclipse-foundation.org")
                    .setName("Project Maker")
                    .build()))
            .setPath("TestProject")
            .setPathWithNamespace("testuser/testproject")
            .setProjectId(69)
            .setProjectVisibility("private")
            .build();

    public static final SystemHook HOOK_MISSING_EVENT = SystemHook.builder()
            .setCreatedAt(ZonedDateTime.now())
            .setUpdatedAt(ZonedDateTime.now())
            .setEventName("")
            .setName("TestProject")
            .setOwnerEmail("testuser@eclipse-foundation.org")
            .setOwnerName("Test User")
            .setOwners(Arrays.asList(Owner.builder()
                    .setEmail("projectmaker@eclipse-foundation.org")
                    .setName("Project Maker")
                    .build()))
            .setPath("TestProject")
            .setPathWithNamespace("testuser/testproject")
            .setProjectId(69)
            .setProjectVisibility("private")
            .build();

    public static final EndpointTestCase CASE_HOOK_SUCCESS = TestCaseHelper
            .prepareTestCase(GITLAB_HOOK_URL, new String[] {}, null)
            .setHeaderParams(Optional.of(Map.of("X-Gitlab-Event", "system hook")))
            .build();

    public static final EndpointTestCase CASE_HOOK_MISSING_HEADER = TestCaseHelper.buildSuccessCase(
            GITLAB_HOOK_URL, new String[] {}, null);

    @Test
    void processCreateHook_success() {
        RestAssuredTemplates.testPost(CASE_HOOK_SUCCESS, PROJECT_CREATE_HOOK_VALID);
    }

    @Test
    void processDeleteHook_success() {
        RestAssuredTemplates.testPost(CASE_HOOK_SUCCESS, PROJECT_DESTROY_HOOK_VALID);
    }

    @Test
    void processRenameHook_success() {
        RestAssuredTemplates.testPost(CASE_HOOK_SUCCESS, PROJECT_RENAME_HOOK_VALID);
    }

    @Test
    void processCreateHook_success_untrackedEvent() {
        RestAssuredTemplates.testPost(CASE_HOOK_SUCCESS, HOOK_NOT_TRACKED);
    }

    @Test
    void processCreateHook_success_missingEventName() {
        RestAssuredTemplates.testPost(CASE_HOOK_SUCCESS, HOOK_MISSING_EVENT);
    }

    @Test
    void processCreateHook_success_missingHeaderParam() {
        RestAssuredTemplates.testPost(CASE_HOOK_MISSING_HEADER, PROJECT_CREATE_HOOK_VALID);
    }
}
