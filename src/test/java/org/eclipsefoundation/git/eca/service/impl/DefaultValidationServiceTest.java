/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class DefaultValidationServiceTest {

    @Inject
    ValidationService validation;

    @Test
    void generateRequestHash_reproducible() {
        ValidationRequest vr = generateBaseRequest();
        String fingerprint = validation.generateRequestHash(vr);
        // if pushing the same set of commits without change the fingerprint won't change
        Assertions.assertEquals(fingerprint, validation.generateRequestHash(vr));
    }

    @Test
    void generateRequestHash_changesWithRepoURL() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        // generate initial fingerprint
        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits)
                .build();
        String fingerprint = validation.generateRequestHash(vr);
        // generate request with different repo url
        vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/other-sample"))
                .setCommits(commits).build();
        // fingerprint should change based on repo URL to reduce risk of collision
        Assertions.assertNotEquals(fingerprint, validation.generateRequestHash(vr));
    }

    @Test
    void generateRequestHash_changesWithNewCommits() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        // generate initial fingerprint
        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits)
                .build();
        String fingerprint = validation.generateRequestHash(vr);
        Commit c2 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c2);
        // generate request with different repo url
        vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/other-sample"))
                .setCommits(commits).build();
        // each commit added should modify the fingerprint at least slightly
        Assertions.assertNotEquals(fingerprint, validation.generateRequestHash(vr));
    }

    @Test
    void getHistoricValidationStatus_noFingerprint() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        Assertions.assertTrue(validation.getHistoricValidationStatus(wrap, null).isEmpty());
        Assertions.assertTrue(validation.getHistoricValidationStatus(wrap, " ").isEmpty());
        Assertions.assertTrue(validation.getHistoricValidationStatus(wrap, "").isEmpty());
    }

    @Test
    void getHistoricValidationStatus_noResultsForFingerprint() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        Assertions.assertTrue(
                validation.getHistoricValidationStatus(wrap, UUID.randomUUID().toString()).isEmpty());
    }

    @Test
    void getHistoricValidationStatus_success() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        Assertions.assertTrue(!validation.getHistoricValidationStatus(wrap, "957706b0f31e0ccfc5287c0ebc62dc79")
                .isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_noneExisting() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        ValidationRequest vr = generateBaseRequest();
        List<CommitValidationStatus> out = validation.getRequestCommitValidationStatus(wrap, vr, "sample.proj");
        // should always return non-null, should be empty w/ no results as there
        // shouldn't be a matching status
        Assertions.assertTrue(out.isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_existing() {
        // create request that lines up with one of the existing test commit validation statuses
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash("123456789")
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);
        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits)
                .build();
        List<CommitValidationStatus> out = validation.getRequestCommitValidationStatus(wrap, vr, "sample.proj");
        // should contain one of the test status objects
        Assertions.assertTrue(!out.isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_noProjectWithResults() {
        // create request that lines up with one of the existing test commit validation statuses
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash("abc123def456")
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);
        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits)
                .build();
        List<CommitValidationStatus> out = validation.getRequestCommitValidationStatus(wrap, vr, null);
        // should contain one of the test status objects
        Assertions.assertTrue(!out.isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_noProjectWithNoResults() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        ValidationRequest vr = generateBaseRequest();
        List<CommitValidationStatus> out = validation.getRequestCommitValidationStatus(wrap, vr, null);
        // should contain one of the test status objects
        Assertions.assertTrue(out.isEmpty());
    }

    /**
     * Used when a random validationRequest is needed and will not need to be
     * recreated/modified. Base request should register as a commit for the test
     * `sample.proj` project.
     * 
     * @return random basic validation request.
     */
    private ValidationRequest generateBaseRequest() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);
        return ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits)
                .build();
    }
}
