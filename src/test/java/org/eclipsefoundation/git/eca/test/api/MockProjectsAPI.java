/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.git.eca.api.ProjectsAPI;
import org.eclipsefoundation.git.eca.api.models.InterestGroupData;
import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.api.models.InterestGroupData.Descriptor;
import org.eclipsefoundation.git.eca.api.models.Project.GithubProject;
import org.eclipsefoundation.git.eca.api.models.Project.GitlabProject;
import org.eclipsefoundation.git.eca.api.models.Project.Repo;
import org.eclipsefoundation.git.eca.api.models.Project.User;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockProjectsAPI implements ProjectsAPI {

    private List<Project> src;

    @PostConstruct
    public void build() {
        this.src = new ArrayList<>();

        // sample repos
        Repo r1 = new Repo();
        r1.setUrl("http://www.github.com/eclipsefdn/sample");
        Repo r2 = new Repo();
        r2.setUrl("http://www.github.com/eclipsefdn/test");
        Repo r3 = new Repo();
        r3.setUrl("http://www.github.com/eclipsefdn/prototype.git");
        Repo r5 = new Repo();
        r5.setUrl("/gitroot/sample/gerrit.project.git");
        Repo r6 = new Repo();
        r6.setUrl("/gitroot/sample/gerrit.other-project");
        Repo r7 = new Repo();
        r7.setUrl("https://gitlab.eclipse.org/eclipse/dash/dash.git");
        Repo r8 = new Repo();
        r8.setUrl("https://gitlab.eclipse.org/eclipse/dash-second/dash.handbook.test");

        // sample users, correlates to users in Mock projects API
        User u1 = User.builder().setUrl("").setUsername("da_wizz").build();
        User u2 = User.builder().setUrl("").setUsername("grunter").build();

        // projects
        Project p1 = Project
                .builder()
                .setName("Sample project")
                .setProjectId("sample.proj")
                .setSpecProjectWorkingGroup(Collections.emptyList())
                .setGithubRepos(Arrays.asList(r1, r2))
                .setGerritRepos(Arrays.asList(r5))
                .setCommitters(Arrays.asList(u1, u2))
                .setProjectLeads(Collections.emptyList())
                .setGitlab(GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList()).setProjectGroup("").build())
                .setGithub(GithubProject.builder().setIgnoredRepos(Collections.emptyList()).setOrg("").build())
                .build();
        src.add(p1);

        Project p2 = Project
                .builder()
                .setName("Prototype thing")
                .setProjectId("sample.proto")
                .setSpecProjectWorkingGroup(Collections.emptyList())
                .setGithubRepos(Arrays.asList(r3))
                .setGerritRepos(Arrays.asList(r6))
                .setGitlabRepos(Arrays.asList(r8))
                .setCommitters(Arrays.asList(u2))
                .setProjectLeads(Collections.emptyList())
                .setGitlab(
                        GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList()).setProjectGroup("eclipse/dash-second").build())
                .setGithub(GithubProject.builder().setIgnoredRepos(Collections.emptyList()).setOrg("").build())
                .build();
        src.add(p2);

        Map<String, String> map = new HashMap<>();
        map.put("id", "proj1");
        Project p3 = Project
                .builder()
                .setName("Spec project")
                .setProjectId("spec.proj")
                .setSpecProjectWorkingGroup(map)
                .setGithubRepos(Collections.emptyList())
                .setGitlabRepos(Arrays.asList(r7))
                .setGitlab(GitlabProject
                        .builder()
                        .setIgnoredSubGroups(Arrays.asList("eclipse/dash/mirror"))
                        .setProjectGroup("eclipse/dash")
                        .build())
                .setGithub(GithubProject
                        .builder()
                        .setIgnoredRepos(Arrays.asList("eclipsefdn-tck/tck-ignored"))
                        .setOrg("eclipsefdn-tck")
                        .build())
                .setCommitters(Arrays.asList(u1, u2))
                .setProjectLeads(Collections.emptyList())
                .build();
        src.add(p3);
    }

    @Override
    public Response getProjects(BaseAPIParameters baseParams) {
        return Response.ok(baseParams.getPage() == 1 ? new ArrayList<>(src) : Collections.emptyList()).build();
    }

    @Override
    public Response getInterestGroups(BaseAPIParameters params) {
        return Response
                .ok(Arrays
                        .asList(InterestGroupData
                                .builder()
                                .setProjectId("foundation-internal.ig.mittens")
                                .setId("1")
                                .setLogo("")
                                .setState("active")
                                .setTitle("Magical IG Tributed To Eclipse News Sources")
                                .setDescription(Descriptor.builder().setFull("Sample").setSummary("Sample").build())
                                .setScope(Descriptor.builder().setFull("Sample").setSummary("Sample").build())
                                .setGitlab(GitlabProject
                                        .builder()
                                        .setIgnoredSubGroups(Collections.emptyList())
                                        .setProjectGroup("eclipse-ig/mittens")
                                        .build())
                                .setLeads(Arrays
                                        .asList(User
                                                .builder()
                                                .setUrl("https://api.eclipse.org/account/profile/zacharysabourin")
                                                .setUsername("zacharysabourin")
                                                .build()))
                                .setParticipants(Arrays
                                        .asList(User
                                                .builder()
                                                .setUrl("https://api.eclipse.org/account/profile/skilpatrick")
                                                .setUsername("skilpatrick")
                                                .build()))
                                .build()))
                .build();
    }
}
