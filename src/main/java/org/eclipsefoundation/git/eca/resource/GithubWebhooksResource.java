/**
 * Copyright (c) 2022, 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.FormParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.helper.CaptchaHelper;
import org.eclipsefoundation.git.eca.model.RevalidationResponse;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.HCaptchaErrorCodes;
import org.jboss.resteasy.annotations.jaxrs.HeaderParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource for processing Github pull request events, used to update commit status entries for the Git ECA application.
 * 
 * @author Martin Lowe
 *
 */
@Path("webhooks/github")
public class GithubWebhooksResource extends GithubAdjacentResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubWebhooksResource.class);

    @Inject
    CaptchaHelper captchaHelper;

    /**
     * Entry point for processing Github webhook requests. Accepts standard fields as described in the <a href=
     * "https://docs.github.com/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#webhook-payload-object-common-properties">Webhook
     * properties documentation</a>, as well as the <a href=
     * "https://docs.github.com/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#pull_request">Pull
     * Request event type documentation.</a>
     * 
     * @param deliveryId Github provided unique delivery ID
     * @param eventType the type of webhook event that was fired
     * @param request the webhook request body.
     * @return an OK status when done processing.
     */
    @POST
    public Response processGithubWebhook(@HeaderParam("X-GitHub-Delivery") String deliveryId,
            @HeaderParam("X-GitHub-Hook-ID") String hookId, @HeaderParam("X-GitHub-Event") String eventType, GithubWebhookRequest request) {
        // If event isn't a pr event, drop as we don't process them
        if (!"pull_request".equalsIgnoreCase(eventType)) {
            return Response.ok().build();
        }
        LOGGER.trace("Processing PR event for install {} with delivery ID of {}", request.getInstallation().getId(), deliveryId);
        // prepare for validation process by pre-processing into standard format
        ValidationRequest vr = generateRequest(request);
        // track the request before we start processing
        retrieveAndUpdateTrackingInformation(request.getInstallation().getId(), request.getRepository().getFullName(),
                request.getPullRequest());
        // process the request
        handleGithubWebhookValidation(request, vr);

        return Response.ok().build();
    }

    /**
     * Endpoint for triggering revalidation of a past request. Uses the fingerprint hash to lookup the unique request and to
     * rebuild the request and revalidate if it exists.
     * 
     * @param fullRepoName the full repository name for the target repo, e.g. eclipse/jetty
     * @param installationId the installation ID for the ECA app in the given repository
     * @param prNo the pull request number to be revalidated
     * @param captchaResponse the passed captcha challenge response
     * @return redirect to the pull request once done processing
     */
    @POST
    @Path("revalidate")
    public Response revalidateWebhookRequest(@QueryParam(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW) String fullRepoName,
            @QueryParam(GitEcaParameterNames.INSTALLATION_ID_RAW) String installationId,
            @QueryParam(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW) Integer prNo,
            @FormParam("h-captcha-response") String captchaResponse) {
        // retrieve and check that the PR exists
        Optional<PullRequest> prResponse = ghAppService.getPullRequest(installationId, fullRepoName, prNo);
        if (prResponse.isEmpty()) {
            throw new NotFoundException(String.format("Cannot find Github PR for repo '%s', pull request number '%d'", fullRepoName, prNo));
        }

        // get the tracking if it exists, create it if it doesn't, and fail out if there is an issue
        GithubWebhookTracking tracking = retrieveAndUpdateTrackingInformation(installationId, fullRepoName, prResponse.get());
        if (tracking == null) {
            throw new ServerErrorException(
                    String.format("Cannot find a tracked pull request with for repo '%s', pull request number '%d'", fullRepoName, prNo),
                    500);
        } else if (!"open".equalsIgnoreCase(tracking.getLastKnownState())) {
            // we do not want to reprocess non-open pull requests
            throw new BadRequestException("Cannot revalidate a non-open pull request");
        }

        // check the captcha challenge response
        List<HCaptchaErrorCodes> errors = captchaHelper.validateCaptchaResponse(captchaResponse);
        if (!errors.isEmpty()) {
            // use debug logging as this could be incredibly noisy
            LOGGER
                    .debug("Captcha challenge failed with the following errors for revalidation request for '{}#{}': {}", fullRepoName,
                            prNo, errors.stream().map(HCaptchaErrorCodes::getMessage));
            throw new BadRequestException("hCaptcha challenge response failed for this request");
        }

        // get the tracking class, convert back to a GH webhook request, and validate the request
        GithubWebhookRequest request = GithubWebhookRequest.buildFromTracking(tracking);
        boolean isSuccessful = handleGithubWebhookValidation(request, generateRequest(request));
        LOGGER.debug("Revalidation for request for '{}#{}' was {}successful.", fullRepoName, prNo, isSuccessful ? "" : " not");

        // build the url for pull request page
        StringBuilder sb = new StringBuilder();
        sb.append("https://github.com/");
        sb.append(tracking.getRepositoryFullName());
        sb.append("/pull/");
        sb.append(tracking.getPullRequestNumber());
        // respond with a URL to the new location in a standard request
        return Response.ok(RevalidationResponse.builder().setLocation(URI.create(sb.toString())).build()).build();
    }

    /**
     * Generate the validation request for the current GH Webhook request.
     * 
     * @param request the current webhook request for validation
     * @return the Validation Request body to be used in validating data
     */
    private ValidationRequest generateRequest(GithubWebhookRequest request) {
        return generateRequest(request.getInstallation().getId(), request.getRepository().getFullName(),
                request.getPullRequest().getNumber(), request.getRepository().getHtmlUrl());
    }

}
