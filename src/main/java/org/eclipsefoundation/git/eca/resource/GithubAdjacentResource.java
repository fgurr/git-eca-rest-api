/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubCommit;
import org.eclipsefoundation.git.eca.api.models.GithubCommit.ParentCommit;
import org.eclipsefoundation.git.eca.api.models.GithubCommitStatusRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.helper.JwtHelper;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.GithubCommitStatuses;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.GithubApplicationService;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains operations and properties that are common to resources that interact with Github validation.
 * 
 * @author Martin Lowe
 *
 */
public abstract class GithubAdjacentResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubAdjacentResource.class);

    private static final String VALIDATION_LOGGING_MESSAGE = "Setting validation state for {}/#{} to {}";

    @ConfigProperty(name = "eclipse.webhooks.github.context")
    String context;
    @ConfigProperty(name = "eclipse.webhooks.github.server-target")
    String serverTarget;
    @ConfigProperty(name = "eclipse.github.default-api-version", defaultValue = "2022-11-28")
    String apiVersion;

    @Inject
    JwtHelper jwtHelper;
    @Inject
    APIMiddleware middleware;
    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Inject
    ValidationService validation;
    @Inject
    GithubApplicationService ghAppService;

    @Inject
    RequestWrapper wrapper;

    @RestClient
    GithubAPI ghApi;

    /**
     * Generate a ValidationRequest object based on data pulled from Github, grabbing commits from the noted pull request
     * using the installation ID for access/authorization.
     * 
     * @param installationId the ECA app installation ID for the organization
     * @param repositoryFullName the full name of the repository where the PR resides
     * @param pullRequestNumber the pull request number that is being validated
     * @param repositoryUrl the URL of the repository that contains the commits to validate
     * @return the populated validation request for the Github request information
     */
    ValidationRequest generateRequest(String installationId, String repositoryFullName, int pullRequestNumber, String repositoryUrl) {
        checkRequestParameters(installationId, repositoryFullName, pullRequestNumber);
        // get the commits that will be validated, don't cache as changes can come in too fast for it to be useful
        List<GithubCommit> commits = middleware
                .getAll(i -> ghApi
                        .getCommits(jwtHelper.getGhBearerString(installationId), apiVersion, repositoryFullName, pullRequestNumber),
                        GithubCommit.class);
        LOGGER.trace("Retrieved {} commits for PR {} in repo {}", commits.size(), pullRequestNumber, repositoryUrl);
        // set up the validation request from current data
        return ValidationRequest
                .builder()
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create(repositoryUrl))
                .setStrictMode(true)
                .setCommits(commits
                        .stream()
                        .map(c -> Commit
                                .builder()
                                .setHash(c.getSha())
                                .setAuthor(GitUser
                                        .builder()
                                        .setMail(c.getCommit().getAuthor().getEmail())
                                        .setName(c.getCommit().getAuthor().getName())
                                        .setExternalId(c.getAuthor().getLogin())
                                        .build())
                                .setCommitter(GitUser
                                        .builder()
                                        .setMail(c.getCommit().getCommitter().getEmail())
                                        .setName(c.getCommit().getCommitter().getName())
                                        .setExternalId(c.getCommitter().getLogin())
                                        .build())
                                .setParents(c.getParents().stream().map(ParentCommit::getSha).collect(Collectors.toList()))
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Shortcut method that will retrieve existing GH tracking info, create new entries if missing, and will update the
     * state of existing requests as well.
     * 
     * @param installationId the installation ID for the ECA app in the given repository
     * @param repositoryFullName the full repository name for the target repo, e.g. eclipse/jetty
     * @param pr the pull request targeted by the validation request.
     * @return a new or updated tracking object, or null if there was an error in saving the information
     */
    GithubWebhookTracking retrieveAndUpdateTrackingInformation(String installationId, String repositoryFullName, PullRequest pr) {
        return updateGithubTrackingIfMissing(getExistingRequestInformation(installationId, repositoryFullName, pr.getNumber()), pr,
                installationId, repositoryFullName);
    }

    /**
     * Attempts to retrieve a webhook tracking record given the installation, repository, and pull request number.
     * 
     * @param installationId the installation ID for the ECA app in the given repository
     * @param repositoryFullName the full repository name for the target repo, e.g. eclipse/jetty
     * @param pullRequestNumber the pull request number that is being processed currently
     * @return the webhook tracking record if it can be found, or an empty optional.
     */
    Optional<GithubWebhookTracking> getExistingRequestInformation(String installationId, String repositoryFullName, int pullRequestNumber) {
        checkRequestParameters(installationId, repositoryFullName, pullRequestNumber);
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.INSTALLATION_ID_RAW, installationId);
        params.add(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW, repositoryFullName);
        params.add(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW, Integer.toString(pullRequestNumber));
        return dao.get(new RDBMSQuery<>(wrapper, filters.get(GithubWebhookTracking.class), params)).stream().findFirst();
    }

    /**
     * Checks if the Github tracking is present for the current request, and if missing will generate a new record and save
     * it.
     * 
     * @param tracking the optional tracking entry for the current request
     * @param request the pull request that is being validated
     * @param installationId the ECA app installation ID for the current request
     * @param fullRepoName the full repo name for the validation request
     */
    GithubWebhookTracking updateGithubTrackingIfMissing(Optional<GithubWebhookTracking> tracking, PullRequest request,
            String installationId, String fullRepoName) {
        // if there is no tracking present, create the missing tracking and persist it
        GithubWebhookTracking updatedTracking;
        if (tracking.isEmpty()) {
            updatedTracking = new GithubWebhookTracking();
            updatedTracking.setHeadSha(request.getHead().getSha());
            updatedTracking.setInstallationId(installationId);
            updatedTracking.setLastUpdated(DateTimeHelper.now());
            updatedTracking.setPullRequestNumber(request.getNumber());
            updatedTracking.setRepositoryFullName(fullRepoName);
            updatedTracking.setLastKnownState(request.getState());
            if (!"open".equalsIgnoreCase(request.getState())) {
                LOGGER
                        .warn("The PR {} in {} is not in an open state, and will not be validated to follow our validation practice",
                                updatedTracking.getPullRequestNumber(), fullRepoName);
            }
        } else {
            // at least update the state on every run
            updatedTracking = tracking.get();
            updatedTracking.setLastKnownState(request.getState());
        }

        // save the data, and log on its success or failure
        List<GithubWebhookTracking> savedTracking = dao
                .add(new RDBMSQuery<>(wrapper, filters.get(GithubWebhookTracking.class)), Arrays.asList(updatedTracking));
        if (savedTracking.isEmpty()) {
            LOGGER.warn("Unable to create new GH tracking record for request to validate {}#{}", fullRepoName, request.getNumber());
            return null;
        }
        // return the updated tracking when successful
        LOGGER.debug("Created new GH tracking record for request to validate {}#{}", fullRepoName, request.getNumber());
        return savedTracking.get(0);
    }

    /**
     * Process the current request and update the checks state to pending then success or failure. Contains verbose TRACE
     * logging for more info on the states of the validation for more information
     * 
     * @param request information about the request from the GH webhook on what resource requested revalidation. Used to
     * target the commit status of the resources
     * @param vr the pseudo request generated from the contextual webhook data. Used to make use of existing validation
     * logic.
     * @return true if the validation passed, false otherwise.
     */
    boolean handleGithubWebhookValidation(GithubWebhookRequest request, ValidationRequest vr) {
        // update the status before processing
        LOGGER
                .trace(VALIDATION_LOGGING_MESSAGE, request.getRepository().getFullName(), request.getPullRequest().getNumber(),
                        GithubCommitStatuses.PENDING);
        updateCommitStatus(request, GithubCommitStatuses.PENDING);

        // validate the response
        LOGGER
                .trace("Begining validation of request for {}/#{}", request.getRepository().getFullName(),
                        request.getPullRequest().getNumber());
        ValidationResponse r = validation.validateIncomingRequest(vr, wrapper);
        if (r.getPassed()) {
            LOGGER
                    .trace(VALIDATION_LOGGING_MESSAGE, request.getRepository().getFullName(), request.getPullRequest().getNumber(),
                            GithubCommitStatuses.SUCCESS);
            updateCommitStatus(request, GithubCommitStatuses.SUCCESS);
            return true;
        }
        LOGGER
                .trace(VALIDATION_LOGGING_MESSAGE, request.getRepository().getFullName(), request.getPullRequest().getNumber(),
                        GithubCommitStatuses.FAILURE);
        updateCommitStatus(request, GithubCommitStatuses.FAILURE);
        return false;
    }

    /**
     * Sends off a POST to update the commit status given a context for the current PR.
     * 
     * @param request the current webhook update request
     * @param state the state to set the status to
     * @param fingerprint the internal unique string for the set of commits being processed
     */
    private void updateCommitStatus(GithubWebhookRequest request, GithubCommitStatuses state) {
        LOGGER
                .trace("Generated access token for installation {}: {}", request.getInstallation().getId(),
                        jwtHelper.getGithubAccessToken(request.getInstallation().getId()).getToken());
        ghApi
                .updateStatus(jwtHelper.getGhBearerString(request.getInstallation().getId()), apiVersion,
                        request.getRepository().getFullName(), request.getPullRequest().getHead().getSha(),
                        GithubCommitStatusRequest
                                .builder()
                                .setDescription(state.getMessage())
                                .setState(state.toString())
                                .setTargetUrl(serverTarget + "/git/eca/status/gh/" + request.getRepository().getFullName() + '/'
                                        + request.getPullRequest().getNumber())
                                .setContext(context)
                                .build());
    }

    /**
     * Validates required fields for processing requests.
     * 
     * @param installationId the installation ID for the ECA app in the given repository
     * @param repositoryFullName the full repository name for the target repo, e.g. eclipse/jetty
     * @param pullRequestNumber the pull request number that is being processed currently
     * @throws BadRequestException if at least one of the parameters is in an invalid state.
     */
    private void checkRequestParameters(String installationId, String repositoryFullName, int pullRequestNumber) {
        List<String> missingFields = new ArrayList<>();
        if (StringUtils.isBlank(installationId)) {
            missingFields.add(GitEcaParameterNames.INSTALLATION_ID_RAW);
        }
        if (StringUtils.isBlank(repositoryFullName)) {
            missingFields.add(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW);
        }
        if (pullRequestNumber < 1) {
            missingFields.add(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW);
        }

        // throw exception if some fields are missing as we can't continue to process the request
        if (!missingFields.isEmpty()) {
            throw new BadRequestException("Missing fields in order to prepare request: " + StringUtils.join(missingFields, ' '));
        }
    }

}
