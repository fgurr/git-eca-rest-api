/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.service.InterestGroupService;
import org.eclipsefoundation.git.eca.service.ProjectsService;
import org.eclipsefoundation.git.eca.service.UserService;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ECA validation endpoint for Git commits. Will use information from the bots, projects, and accounts API to validate
 * commits passed to this endpoint. Should be as system agnostic as possible to allow for any service to request
 * validation with less reliance on services external to the Eclipse foundation.
 *
 * @author Martin Lowe, Zachary Sabourin
 */
@Path("/eca")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class ValidationResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationResource.class);

    @Inject
    @ConfigProperty(name = "eclipse.mail.allowlist")
    List<String> allowListUsers;
    @Inject
    @ConfigProperty(name = "eclipse.noreply.email-patterns")
    List<String> emailPatterns;

    @Inject
    RequestWrapper wrapper;

    // external API/service harnesses
    @Inject
    CachingService cache;
    @Inject
    ProjectsService projects;
    @Inject
    UserService users;
    @Inject
    ValidationService validation;
    @Inject
    InterestGroupService ig;

    /**
     * Consuming a JSON request, this method will validate all passed commits, using the repo URL and the repository
     * provider. These commits will be validated to ensure that all users are covered either by an ECA, or are committers on
     * the project. In the case of ECA-only contributors, an additional sign off footer is required in the body of the
     * commit.
     *
     * @param req the request containing basic data plus the commits to be validated
     * @return a web response indicating success or failure for each commit, along with standard messages that may be used
     * to give users context on failure.
     */
    @POST
    public Response validate(ValidationRequest req) {
        List<String> messages = checkRequest(req);
        // only process if we have no errors
        if (messages.isEmpty()) {
            LOGGER.debug("Processing: {}", req);
            return validation.validateIncomingRequest(req, wrapper).toResponse();
        } else {
            // create a stubbed response with the errors
            ValidationResponse out = ValidationResponse.builder().build();
            messages.forEach(m -> out.addError(m, null, APIStatusCode.ERROR_DEFAULT));
            return out.toResponse();
        }
    }

    @GET
    @Path("/lookup")
    public Response getUserStatus(@QueryParam("email") String email) {
        EclipseUser user = users.getUser(email);
        if (Objects.isNull(user)) {
            return Response.status(404).build();
        }

        if (!user.getECA().getSigned()) {
            return Response.status(403).build();
        }
        return Response.ok().build();
    }

    /**
     * Check if there are any issues with the validation request, returning error messages if there are issues with the
     * request.
     * 
     * @param req the current validation request
     * @return a list of error messages to report, or an empty list if there are no errors with the request.
     */
    private List<String> checkRequest(ValidationRequest req) {
        // check that we have commits to validate
        List<String> messages = new ArrayList<>();
        if (req.getCommits() == null || req.getCommits().isEmpty()) {
            messages.add("A commit is required to validate");
        }
        // check that we have a repo set
        if (req.getRepoUrl() == null) {
            messages.add("A base repo URL needs to be set in order to validate");
        }
        // check that we have a type set
        if (req.getProvider() == null) {
            messages.add("A provider needs to be set to validate a request");
        }
        return messages;
    }

}
