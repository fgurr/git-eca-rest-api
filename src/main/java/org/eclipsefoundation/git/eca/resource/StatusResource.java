/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.service.ProjectsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.qute.Location;
import io.quarkus.qute.Template;

/**
 * REST resource containing endpoints related to checking the status of validation requests.
 * 
 * @author Martin Lowe
 *
 */
@Path("eca/status")
public class StatusResource extends GithubAdjacentResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatusResource.class);

    @Inject
    ProjectsService projects;

    // Qute templates, generates UI status page
    @Location("simple_fingerprint_ui")
    Template statusUiTemplate;

    /**
     * Standard endpoint for retrieving raw validation information on a historic request, using the fingerprint for lookups.
     * 
     * @param fingerprint the associated fingerprint with the request that was validated.
     * @return list of commits that exist for the given fingerprint, or empty if there are none that match
     */
    @GET
    @Path("{fingerprint}")
    public Response getCommitValidation(@PathParam("fingerprint") String fingerprint) {
        return Response.ok(validation.getHistoricValidationStatus(wrapper, fingerprint)).build();
    }

    /**
     * Retrieves commit status information based on the fingerprint and builds a UI around the results for easier
     * consumption.
     * 
     * @param fingerprint the string associated with the request for looking up related commit statuses.
     * @return the HTML UI for the status of the fingerprint request
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("{fingerprint}/ui")
    public Response getCommitValidationUI(@PathParam("fingerprint") String fingerprint) {
        List<CommitValidationStatus> statuses = validation.getHistoricValidationStatus(wrapper, fingerprint);
        if (statuses.isEmpty()) {
            return Response.status(404).build();
        }
        List<Project> ps = projects.retrieveProjectsForRepoURL(statuses.get(0).getRepoUrl(), statuses.get(0).getProvider());
        return Response
                .ok()
                .entity(statusUiTemplate
                        .data("statuses", statuses)
                        .data("pullRequestNumber", null)
                        .data("fullRepoName", null)
                        .data("project", ps.isEmpty() ? null : ps.get(0))
                        .data("repoUrl", statuses.get(0).getRepoUrl())
                        .data("installationId", null)
                        .render())
                .build();
    }

    /**
     * Retrieves and checks the validity of the commit statuses for a Github pull request, and if out of date will
     * revalidate the request automatically on load.
     * 
     * @param org the organization in Github that contains the target repo
     * @param repoName the name of the repo in Github containing the pull request
     * @param prNo the number of the pull request being targeted for the validation lookup
     * @return the status UI for the Github status lookup, or an error if there was a problem
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("gh/{org}/{repoName}/{prNo}")
    public Response getCommitValidationForGithub(@PathParam("org") String org, @PathParam("repoName") String repoName,
            @PathParam("prNo") Integer prNo) {
        String fullRepoName = org + '/' + repoName;
        // get the installation ID for the given repo if it exists, and if the PR noted exists
        String installationId = ghAppService.getInstallationForRepo(fullRepoName);
        Optional<PullRequest> prResponse = ghAppService.getPullRequest(installationId, fullRepoName, prNo);
        if (StringUtils.isBlank(installationId)) {
            throw new BadRequestException("Could not find an ECA app installation for repo name: " + fullRepoName);
        } else if (prResponse.isEmpty()) {
            throw new NotFoundException(String.format("Could not find PR '%d' in repo name '%s'", prNo, fullRepoName));
        }

        // prepare the request for consumption
        String repoUrl = "https://github.com/" + fullRepoName;
        ValidationRequest vr = generateRequest(installationId, fullRepoName, prNo, repoUrl);

        // build the commit sha list based on the prepared request
        List<String> commitShas = vr.getCommits().stream().map(Commit::getHash).collect(Collectors.toList());
        // there should always be commits for a PR, but in case, lets check
        if (commitShas.isEmpty()) {
            throw new BadRequestException(String.format("Could not find any commits for %s#%d", fullRepoName, prNo));
        }
        LOGGER.debug("Found {} commits for '{}#{}'", commitShas.size(), fullRepoName, prNo);

        // retrieve the webhook tracking info, or generate an entry to track this PR if it's missing.
        GithubWebhookTracking updatedTracking = retrieveAndUpdateTrackingInformation(installationId, fullRepoName, prResponse.get());
        if (updatedTracking == null) {
            throw new ServerErrorException("Error while attempting to revalidate request, try again later.", 500);
        }

        // get the commit status of commits to use for checking historic validation
        List<CommitValidationStatus> statuses = validation.getHistoricValidationStatusByShas(wrapper, commitShas);
        if (!"open".equalsIgnoreCase(prResponse.get().getState()) && statuses.isEmpty()) {
            throw new BadRequestException("Cannot find validation history for current non-open PR, cannot provide validation status");
        }

        // we only want to update/revalidate for open PRs, so don't do this check if the PR is merged/closed
        if ("open".equalsIgnoreCase(prResponse.get().getState()) && commitShas.size() != statuses.size()) {
            LOGGER.debug("Validation for {}#{} does not seem to be current, revalidating commits", fullRepoName, prNo);
            // using the updated tracking, perform the validation
            handleGithubWebhookValidation(GithubWebhookRequest.buildFromTracking(updatedTracking), vr);
            // call to retrieve the statuses once again since they will have changed at this point
            statuses = validation.getHistoricValidationStatusByShas(wrapper, commitShas);
        }

        // get projects for use in status UI
        List<Project> ps = projects.retrieveProjectsForRepoURL(statuses.get(0).getRepoUrl(), statuses.get(0).getProvider());
        // render and return the status UI
        return Response
                .ok()
                .entity(statusUiTemplate
                        .data("statuses", statuses)
                        .data("pullRequestNumber", prNo)
                        .data("fullRepoName", fullRepoName)
                        .data("project", ps.isEmpty() ? null : ps.get(0))
                        .data("repoUrl", repoUrl)
                        .data("installationId", installationId)
                        .render())
                .build();
    }

}
