/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.time.LocalDate;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.ReportsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/reports")
public class ReportsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportsResource.class);

    @ConfigProperty(name = "eclipse.reports.access-key")
    String key;

    @Inject
    RequestWrapper wrap;
    @Inject
    ReportsService reportsService;

    @GET
    @Path("/gitlab/private-projects")
    public Response getPrivateProjectEvents(@QueryParam("key") String passedKey, @QueryParam("status") String status,
            @QueryParam("since") LocalDate since, @QueryParam("until") LocalDate until) {
        if (!key.equals(passedKey)) {
            LOGGER.debug("Bad key passed for access, access blocked");
            return Response.status(401).build();
        }
        if (StringUtils.isNotBlank(status) && !isValidStatus(status)) {
            throw new BadRequestException(String.format("Invalid 'status' parameter: %s", status));
        }

        return Response.ok(reportsService.getPrivateProjectEvents(wrap, status, since, until)).build();
    }

    /**
     * Validates the status as one of "active" or "deleted"
     * 
     * @param status the desired status
     * @return true if valid, false if not
     */
    private boolean isValidStatus(String status) {
        return status.equalsIgnoreCase(GitEcaParameterNames.STATUS_ACTIVE.getName())
                || status.equalsIgnoreCase(GitEcaParameterNames.STATUS_DELETED.getName());
    }
}
