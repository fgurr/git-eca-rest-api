/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.helper;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

/**
 * Contains helpers for processing commits.
 *
 * @author Martin Lowe
 *
 */
public class CommitHelper {

    /**
     * Validate the commits fields.
     *
     * @param c commit to validate
     * @return true if valid, otherwise false
     */
    public static boolean validateCommit(Commit c) {
        if (c == null) {
            return false;
        }

        boolean valid = true;
        // check current commit data
        if (c.getHash() == null) {
            valid = false;
        }
        // check author
        if (c.getAuthor() == null || c.getAuthor().getMail() == null) {
            valid = false;
        }
        // check committer
        if (c.getCommitter() == null || c.getCommitter().getMail() == null) {
            valid = false;
        }

        return valid;
    }

    public static MultivaluedMap<String, String> getCommitParams(ValidationRequest req, String projectId) {
        return getCommitParams(req.getCommits().stream().map(Commit::getHash).collect(Collectors.toList()), projectId,
                req.getRepoUrl().toString());
    }

    public static MultivaluedMap<String, String> getCommitParams(List<String> commitShas, String projectId,
            String repoUrl) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.put(GitEcaParameterNames.SHAS_RAW, commitShas);
        params.add(GitEcaParameterNames.REPO_URL_RAW, repoUrl);
        if (projectId != null) {
            params.add(GitEcaParameterNames.PROJECT_ID_RAW, projectId);
        }
        return params;
    }

    /**
     * Centralized way of retrieving a checked project ID from a project for use
     * when interacting with commits and commit data.
     * 
     * @param p the current project to attempt to retrieve an ID from
     * @return the project ID or the given default (empty string).
     */
    public static String getProjectId(Project p) {
        return p != null ? p.getProjectId() : "";
    }

    private CommitHelper() {
    }
}
