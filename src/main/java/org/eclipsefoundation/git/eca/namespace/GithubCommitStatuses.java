/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.namespace;

/**
 * Statuses used for Github commit status objects. Contains basic messaging used in the description of the status to be
 * sent back to Github.
 * 
 * @author Martin Lowe
 *
 */
public enum GithubCommitStatuses {

    SUCCESS("The author(s) of the pull request is covered by necessary legal agreements in order to proceed!"),
    FAILURE("The author(s) of the pull request is not covered by necessary legal agreements in order to proceed."),
    ERROR("An unexpected error has occurred. Please contact webmaster@eclipse.org."),
    PENDING("Eclipse Foundation Contributor Agreement validation is in progress.");

    private String message;

    /**
     * Instantiate the enum status with the display message.
     * 
     * @param message the display message associated with the status.
     */
    private GithubCommitStatuses(String message) {
        this.message = message;
    }

    /**
     * Returns the message associated with the status.
     * 
     * @return the string message for status.
     */
    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
