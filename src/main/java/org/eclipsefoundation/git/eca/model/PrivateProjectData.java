/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model;

import java.time.LocalDateTime;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Model for Gitlab private project report logs. Used to report on historic projects in the EF hosted Gitlab instance.
 * 
 * @author Zachary Sabourin
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_PrivateProjectData.Builder.class)
public abstract class PrivateProjectData {

    public abstract Integer getUserId();

    public abstract Integer getProjectId();

    public abstract String getProjectPath();

    @JsonProperty("ef_username")
    @Nullable
    public abstract String getEFUsername();

    @Nullable
    public abstract Integer getParentProject();

    public abstract LocalDateTime getCreationDate();

    @Nullable
    public abstract LocalDateTime getDeletionDate();

    public static Builder builder() {
        return new AutoValue_PrivateProjectData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setUserId(Integer id);

        public abstract Builder setProjectId(Integer id);

        public abstract Builder setProjectPath(String path);

        public abstract Builder setEFUsername(@Nullable String username);

        public abstract Builder setParentProject(@Nullable Integer id);

        public abstract Builder setCreationDate(LocalDateTime time);

        public abstract Builder setDeletionDate(@Nullable LocalDateTime time);

        public abstract PrivateProjectData build();
    }
}
