/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.service.impl;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubApplicationInstallation;
import org.eclipsefoundation.git.eca.api.models.GithubInstallationRepositoriesResponse;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.helper.JwtHelper;
import org.eclipsefoundation.git.eca.service.GithubApplicationService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;

/**
 * Default caching implementation of the GH app service. This uses a loading cache to keep installation info highly
 * available to reduce latency in calls.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultGithubApplicationService implements GithubApplicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultGithubApplicationService.class);

    @ConfigProperty(name = "eclipse.github.default-api-version", defaultValue = "2022-11-28")
    String apiVersion;

    @RestClient
    GithubAPI gh;

    @Inject
    JwtHelper jwt;
    @Inject
    CachingService cache;
    @Inject
    APIMiddleware middle;

    @Inject
    ManagedExecutor exec;

    private AsyncLoadingCache<String, MultivaluedMap<String, String>> installationRepositoriesCache;

    @PostConstruct
    void init() {
        this.installationRepositoriesCache = Caffeine
                .newBuilder()
                .executor(exec)
                .maximumSize(10)
                .refreshAfterWrite(Duration.ofMinutes(60))
                .buildAsync(k -> loadInstallationRepositories());
        // do initial map population
        getAllInstallRepos();
    }

    @Override
    public String getInstallationForRepo(String repoFullName) {
        MultivaluedMap<String, String> map = getAllInstallRepos();
        return map.keySet().stream().filter(k -> map.get(k).contains(repoFullName)).findFirst().orElse(null);
    }

    private MultivaluedMap<String, String> getAllInstallRepos() {
        try {
            return this.installationRepositoriesCache.get("all").get(10L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error("Thread interrupted while building repository cache, no entries will be available for current call");
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            // rewrap exception and throw
            throw new RuntimeException(e);
        }
        return new MultivaluedMapImpl<>();
    }

    @Override
    public Optional<PullRequest> getPullRequest(String installationId, String repoFullName, Integer pullRequest) {
        return cache
                .get(repoFullName, new MultivaluedMapImpl<>(), PullRequest.class,
                        () -> gh.getPullRequest(jwt.getGhBearerString(installationId), apiVersion, repoFullName, pullRequest));
    }

    /**
     * Retrieves a fresh copy of installation repositories, mapped by installation ID to associated full repo names.
     * 
     * @return a multivalued map relating installation IDs to associated full repo names.
     */
    private MultivaluedMap<String, String> loadInstallationRepositories() {
        // create map early for potential empty returns
        MultivaluedMapImpl<String, String> out = new MultivaluedMapImpl<>();
        // get JWT bearer and then get all associated installations of current app
        String auth = "Bearer " + jwt.generateJwt();
        List<GithubApplicationInstallation> installations = middle
                .getAll(i -> gh.getInstallations(i, auth), GithubApplicationInstallation.class);
        // check that there are installations
        if (installations.isEmpty()) {
            LOGGER.warn("Did not find any installations for the currently configured Github application");
            return out;
        }
        // trace log the installations for more context
        LOGGER.trace("Found the following installations: {}", installations);

        // from installations, get the assoc. repos and grab their full repo name and collect them
        installations
                .stream()
                .forEach(installation -> middle
                        .getAll(i -> gh.getInstallationRepositories(i, jwt.getGhBearerString(Integer.toString(installation.getId()))),
                                GithubInstallationRepositoriesResponse.class)
                        .stream()
                        .forEach(installRepo -> installRepo
                                .getRepositories()
                                .stream()
                                .forEach(r -> out.add(Integer.toString(installation.getId()), r.getFullName()))));
        LOGGER.trace("Final results for generating installation mapping: {}", out);
        return out;
    }

}
