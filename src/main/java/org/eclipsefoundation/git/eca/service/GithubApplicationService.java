/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.service;

import java.util.Optional;

import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;

/**
 * Service for interacting with the Github API with caching for performance.
 * 
 * @author Martin Lowe
 *
 */
public interface GithubApplicationService {

    /**
     * Retrieves the installation ID for the ECA app on the given repo if it exists.
     * 
     * @param repoFullName the full repo name to retrieve an installation ID for. E.g. eclipse/jetty
     * @return the numeric installation ID if it exists, or null
     */
    String getInstallationForRepo(String repoFullName);

    /**
     * Retrieves a pull request given the repo, pull request, and associated installation to action the fetch.
     * 
     * @param installationId installation ID to use when creating access tokens to query GH API
     * @param repoFullName the full repo name, where the org and repo name are joined by a slash, e.g. eclipse/jetty
     * @param pullRequest the pull request numeric ID
     * @return the pull request if it exists, otherwise empty
     */
    Optional<PullRequest> getPullRequest(String installationId, String repoFullName, Integer pullRequest);
}
