/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.api.ProjectsAPI;
import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.InterestGroupService;
import org.eclipsefoundation.git.eca.service.ProjectsService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;

import io.quarkus.runtime.Startup;

/**
 * Projects service implementation that handles pagination of data manually, as well as makes use of a loading cache to
 * have data be always available with as little latency to the user as possible.
 *
 * @author Martin Lowe
 * @author Zachary Sabourin
 */
@Startup
@ApplicationScoped
public class PaginationProjectsService implements ProjectsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaginationProjectsService.class);

    @ConfigProperty(name = "eclipse.projects.precache.enabled", defaultValue = "true")
    boolean isEnabled;
    @ConfigProperty(name = "cache.pagination.refresh-frequency-seconds", defaultValue = "3600")
    long refreshAfterWrite;

    @Inject
    @RestClient
    ProjectsAPI projects;
    @Inject
    InterestGroupService ig;
    @Inject
    CachingService cache;
    @Inject
    APIMiddleware middleware;

    @Inject
    ManagedExecutor exec;
    // this class has a separate cache as this data is long to load and should be
    // always available.
    LoadingCache<String, List<Project>> internalCache;

    /**
     * Initializes the internal loader cache and pre-populates the data with the one available key. If more than one key is
     * used, eviction of previous results will happen and create degraded performance.
     */
    @PostConstruct
    public void init() {
        // set up the internal cache
        this.internalCache = CacheBuilder
                .newBuilder()
                .maximumSize(1)
                .refreshAfterWrite(refreshAfterWrite, TimeUnit.SECONDS)
                .build(new CacheLoader<String, List<Project>>() {
                    @Override
                    public List<Project> load(String key) throws Exception {
                        return getProjectsInternal();
                    }

                    /**
                     * Implementation required for refreshAfterRewrite to be async rather than sync and blocking while awaiting for
                     * expensive reload to complete.
                     */
                    @Override
                    public ListenableFuture<List<Project>> reload(String key, List<Project> oldValue) throws Exception {
                        ListenableFutureTask<List<Project>> task = ListenableFutureTask.create(() -> {
                            LOGGER.debug("Retrieving new project data async");
                            List<Project> newProjects = oldValue;
                            try {
                                newProjects = getProjectsInternal();
                            } catch (Exception e) {
                                LOGGER.error("Error while reloading internal projects data, data will be stale for current cycle.", e);
                            }
                            LOGGER.debug("Done refreshing project values");
                            return newProjects;
                        });
                        // run the task using the Quarkus managed executor
                        exec.execute(task);
                        return task;
                    }
                });

        if (isEnabled) {
            // pre-cache the projects to reduce load time for other users
            LOGGER.debug("Starting pre-cache of projects");
            if (getProjects() == null) {
                LOGGER.warn("Unable to populate pre-cache for Eclipse projects. Calls may experience degraded performance.");
            }
            LOGGER.debug("Completed pre-cache of projects assets");
        }
    }

    @Override
    public List<Project> getProjects() {
        try {
            return internalCache.get("projects");
        } catch (ExecutionException e) {
            throw new RuntimeException("Could not load Eclipse projects", e);
        }
    }

    @Override
    public List<Project> retrieveProjectsForRequest(ValidationRequest req) {
        String repoUrl = req.getRepoUrl().getPath();
        if (repoUrl == null) {
            LOGGER.warn("Can not match null repo URL to projects");
            return Collections.emptyList();
        }
        return retrieveProjectsForRepoURL(repoUrl, req.getProvider());
    }

    @Override
    public List<Project> retrieveProjectsForRepoURL(String repoUrl, ProviderType provider) {
        if (repoUrl == null) {
            LOGGER.warn("Can not match null repo URL to projects");
            return Collections.emptyList();
        }
        // check for all projects that make use of the given repo
        List<Project> availableProjects = getProjects();
        availableProjects
                .addAll(cache
                        .get("all", new MultivaluedMapImpl<>(), Project.class, () -> ig.adaptInterestGroups(ig.getInterestGroups()))
                        .orElse(Collections.emptyList()));
        if (availableProjects.isEmpty()) {
            LOGGER.warn("Could not find any projects to match against");
            return Collections.emptyList();
        }
        LOGGER.debug("Checking projects for repos that end with: {}", repoUrl);

        String projectNamespace = URI.create(repoUrl).getPath().substring(1).toLowerCase();
        // filter the projects based on the repo URL. At least one repo in project must
        // match the repo URL to be valid
        switch (provider) {
            case GITLAB:
                return availableProjects
                        .stream()
                        .filter(p -> projectNamespace.startsWith(p.getGitlab().getProjectGroup() + "/")
                                && p.getGitlab().getIgnoredSubGroups().stream().noneMatch(sg -> projectNamespace.startsWith(sg + "/")))
                        .collect(Collectors.toList());
            case GITHUB:
                return availableProjects
                        .stream()
                        .filter(p -> p.getGithubRepos().stream().anyMatch(re -> re.getUrl() != null && re.getUrl().endsWith(repoUrl))
                                || (StringUtils.isNotBlank(p.getGithub().getOrg()) && projectNamespace.startsWith(p.getGithub().getOrg())
                                        && p.getGithub().getIgnoredRepos().stream().noneMatch(repoUrl::endsWith)))
                        .collect(Collectors.toList());
            case GERRIT:
                return availableProjects
                        .stream()
                        .filter(p -> p.getGerritRepos().stream().anyMatch(re -> re.getUrl() != null && re.getUrl().endsWith(repoUrl)))
                        .collect(Collectors.toList());
            default:
                return Collections.emptyList();
        }
    }

    /**
     * Logic for retrieving projects from API.
     *
     * @return list of projects for the cache
     */
    private List<Project> getProjectsInternal() {

        return middleware.getAll(params -> projects.getProjects(params), Project.class).stream().map(proj -> {
            proj.getGerritRepos().forEach(repo -> {
                if (repo.getUrl().endsWith(".git")) {
                    repo.setUrl(repo.getUrl().substring(0, repo.getUrl().length() - 4));
                }
            });
            return proj;
        }).collect(Collectors.toList());
    }
}
