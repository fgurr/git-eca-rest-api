/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;

import io.undertow.util.HexConverter;

/**
 * Service containing logic for validating commits, and retrieving/updating previous validation statuses.
 * 
 * @author Martin Lowe
 *
 */
public interface ValidationService {

    /**
     * Validate an incoming request, checking to make sure that ECA is present for users interacting with the API, as well
     * as maintain access rights to spec projects, blocking non-elevated access to the repositories.
     * 
     * @param req the request to validate
     * @param wrapper the current request wrapper
     * @return the validation results to return to the user.
     */
    public ValidationResponse validateIncomingRequest(ValidationRequest req, RequestWrapper wrapper);

    /**
     * Retrieves a set of validation status objects given the validation request fingerprint.
     * 
     * @param wrapper current request wrapper object
     * @param fingerprint the validation request fingerprint
     * @return the list of historic validation status objects, or an empty list.
     */
    public List<CommitValidationStatus> getHistoricValidationStatus(RequestWrapper wrapper, String fingerprint);

    /**
     * Retrieves a set of validation status objects given the target shas.
     * 
     * @param wrapper current request wrapper object
     * @param shas list of shas to use when fetching historic commit statuses
     * @return the list of historic validation status objects, or an empty list.
     */
    public List<CommitValidationStatus> getHistoricValidationStatusByShas(RequestWrapper wrapper, List<String> shas);

    /**
     * Retrieves a set of commit validation status objects given a validation request and target project.
     * 
     * @param wrapper current request wrapper object
     * @param req the current validation request
     * @param projectId the project targeted by the validation request
     * @return the list of existing validation status objects for the validation request, or an empty list.
     */
    public List<CommitValidationStatus> getRequestCommitValidationStatus(RequestWrapper wrapper, ValidationRequest req, String projectId);

    /**
     * Updates or creates validation status objects for the commits validated as part of the current validation request.
     * Uses information from both the original request and the final response to generate details to be preserved in commit
     * status objects.
     * 
     * @param wrapper current request wrapper object
     * @param r the final validation response
     * @param req the current validation request
     * @param statuses list of existing commit validation objects to update
     * @param p the project targeted by the validation request.
     */
    public void updateCommitValidationStatus(RequestWrapper wrapper, ValidationResponse r, ValidationRequest req,
            List<CommitValidationStatus> statuses, Project p);

    /**
     * Generates a request fingerprint for looking up requests that have already been processed in the past. Collision here
     * is extremely unlikely, and low risk on the change it does. For that reason, a more secure but heavier hashing alg.
     * wasn't chosen.
     * 
     * @param req the request to generate a fingerprint for
     * @return the fingerprint for the request
     */
    default String generateRequestHash(ValidationRequest req) {
        StringBuilder sb = new StringBuilder();
        sb.append(req.getRepoUrl());
        req.getCommits().forEach(c -> sb.append(c.getHash()));
        try {
            return HexConverter.convertToHexString(MessageDigest.getInstance("MD5").digest(sb.toString().getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error while encoding request fingerprint - couldn't find MD5 algorithm.");
        }
    }
}
