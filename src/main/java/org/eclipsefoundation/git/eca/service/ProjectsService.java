/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service;

import java.util.List;

import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.ProviderType;

/**
 * Intermediate layer between resource and API layers that handles retrieval of all projects and caching of that data
 * for availability purposes.
 * 
 * @author Martin Lowe
 *
 */
public interface ProjectsService {

    /**
     * Retrieves all currently available projects from cache if available, otherwise going to API to retrieve a fresh copy
     * of the data.
     * 
     * @return list of projects available from API.
     */
    List<Project> getProjects();

    /**
     * Retrieves projects valid for the current request, or an empty list if no data or matching project repos could be
     * found.
     *
     * @param req the current request
     * @return list of matching projects for the current request, or an empty list if none found.
     */
    List<Project> retrieveProjectsForRequest(ValidationRequest req);

    /**
     * Retrieves projects for given provider, using the repo URL to match to a stored repository.
     * 
     * @param repoUrl the repo URL to match
     * @param provider the provider that is being served for the request.
     * @return a list of matching projects, or an empty list if none are found.
     */
    List<Project> retrieveProjectsForRepoURL(String repoUrl, ProviderType provider);
}
