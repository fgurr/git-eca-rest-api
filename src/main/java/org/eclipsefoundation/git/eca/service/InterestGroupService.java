package org.eclipsefoundation.git.eca.service;

import java.util.List;

import org.eclipsefoundation.git.eca.api.models.InterestGroupData;
import org.eclipsefoundation.git.eca.api.models.Project;

/**
 * Service for retrieving and interacting with interest groups.
 * 
 * @author Martin Lowe
 *
 */
public interface InterestGroupService {

    /**
     * Retrieve all available interest groups.
     * 
     * @return list of all available interest groups
     */
    List<InterestGroupData> getInterestGroups();

    /**
     * Converts interest groups into projects for processing downstream.
     * 
     * @param igs the interest groups to convert
     * @return the converted interest groups
     */
    List<Project> adaptInterestGroups(List<InterestGroupData> igs);
}
