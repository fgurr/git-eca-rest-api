package org.eclipsefoundation.git.eca.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.api.ProjectsAPI;
import org.eclipsefoundation.git.eca.api.models.InterestGroupData;
import org.eclipsefoundation.git.eca.api.models.Project;
import org.eclipsefoundation.git.eca.service.InterestGroupService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

/**
 * Default implementation of interest group service.
 * 
 * @author Martin Lowe
 */
@ApplicationScoped
public class DefaultInterestGroupService implements InterestGroupService {

    @Inject
    APIMiddleware middleware;
    @Inject
    CachingService cache;

    @Inject
    @RestClient
    ProjectsAPI api;

    @Override
    public List<InterestGroupData> getInterestGroups() {
        return cache
                .get("all", new MultivaluedMapImpl<>(), InterestGroupData.class,
                        () -> middleware.getAll(api::getInterestGroups, InterestGroupData.class))
                .orElse(Collections.emptyList());
    }

    @Override
    public List<Project> adaptInterestGroups(List<InterestGroupData> igs) {
        return igs
                .stream()
                .map(ig -> Project
                        .builder()
                        .setProjectId(ig.getProjectId())
                        .setGerritRepos(Collections.emptyList())
                        .setGithubRepos(Collections.emptyList())
                        .setGitlabRepos(Collections.emptyList())
                        .setRepos(Collections.emptyList())
                        .setGitlab(ig.getGitlab())
                        .setCommitters(ig.getParticipants())
                        .setProjectLeads(ig.getLeads())
                        .setName(ig.getTitle())
                        .setSpecProjectWorkingGroup(Collections.emptyMap())
                        .build())
                .collect(Collectors.toList());
    }

}
