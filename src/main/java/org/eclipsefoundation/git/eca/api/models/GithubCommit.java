/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GithubCommit.Builder.class)
public abstract class GithubCommit {
    public abstract String getSha();

    public abstract CommitData getCommit();

    public abstract CommitUser getCommitter();

    public abstract CommitUser getAuthor();

    public abstract List<ParentCommit> getParents();

    public static Builder builder() {
        return new AutoValue_GithubCommit.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setSha(String sha);

        public abstract Builder setCommit(CommitData commit);

        public abstract Builder setCommitter(CommitUser committer);

        public abstract Builder setAuthor(CommitUser author);

        public abstract Builder setParents(List<ParentCommit> parents);

        public abstract GithubCommit build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubCommit_CommitData.Builder.class)
    public abstract static class CommitData {
        public abstract BriefCommitUser getAuthor();

        public abstract BriefCommitUser getCommitter();

        public static Builder builder() {
            return new AutoValue_GithubCommit_CommitData.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setAuthor(BriefCommitUser author);

            public abstract Builder setCommitter(BriefCommitUser committer);

            public abstract CommitData build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubCommit_BriefCommitUser.Builder.class)
    public abstract static class BriefCommitUser {
        public abstract String getName();

        public abstract String getEmail();

        public static Builder builder() {
            return new AutoValue_GithubCommit_BriefCommitUser.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setName(String name);

            public abstract Builder setEmail(String email);

            public abstract BriefCommitUser build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubCommit_CommitUser.Builder.class)
    public abstract static class CommitUser {
        public abstract String getLogin();

        public static Builder builder() {
            return new AutoValue_GithubCommit_CommitUser.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setLogin(String login);

            public abstract CommitUser build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubCommit_ParentCommit.Builder.class)
    public abstract static class ParentCommit {
        public abstract String getSha();

        public static Builder builder() {
            return new AutoValue_GithubCommit_ParentCommit.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setSha(String sha);

            public abstract ParentCommit build();
        }
    }
}
