/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;

/**
 * Represents a project in the Eclipse API, along with the users and repos that exist within the context of the project.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = $AutoValue_Project.Builder.class)
public abstract class Project {
    public abstract String getProjectId();

    public abstract String getName();

    public abstract List<User> getCommitters();

    public abstract List<User> getProjectLeads();

    @Nullable
    public abstract List<Repo> getRepos();

    public abstract List<Repo> getGitlabRepos();

    public abstract List<Repo> getGithubRepos();

    public abstract List<Repo> getGerritRepos();

    public abstract Object getSpecProjectWorkingGroup();

    public abstract GitlabProject getGitlab();

    public abstract GithubProject getGithub();

    @Nullable
    @Memoized
    public String getSpecWorkingGroup() {
        // stored as map as empty returns an array instead of a map
        Object specProjectWorkingGroup = getSpecProjectWorkingGroup();
        if (specProjectWorkingGroup instanceof Map) {
            // we checked in line above that the map exists, so we can safely cast it
            @SuppressWarnings("unchecked")
            Object raw = ((Map<Object, Object>) specProjectWorkingGroup).get("id");
            if (raw instanceof String) {
                return (String) raw;
            }
        }
        return null;
    }

    public static Builder builder() {
        // adds empty lists as default values
        return new AutoValue_Project.Builder()
                .setRepos(new ArrayList<>())
                .setCommitters(new ArrayList<>())
                .setGithubRepos(new ArrayList<>())
                .setGitlabRepos(new ArrayList<>())
                .setGerritRepos(new ArrayList<>());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setProjectId(String projectId);

        public abstract Builder setName(String name);

        public abstract Builder setProjectLeads(List<User> projectLeads);

        public abstract Builder setCommitters(List<User> committers);

        public abstract Builder setRepos(@Nullable List<Repo> repos);

        public abstract Builder setGitlabRepos(List<Repo> gitlabRepos);

        public abstract Builder setGithubRepos(List<Repo> githubRepos);

        public abstract Builder setGerritRepos(List<Repo> gerritRepos);

        public abstract Builder setSpecProjectWorkingGroup(Object specProjectWorkingGroup);

        public abstract Builder setGitlab(GitlabProject gitlab);

        public abstract Builder setGithub(GithubProject github);

        public abstract Project build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_Project_User.Builder.class)
    public abstract static class User {
        public abstract String getUsername();

        public abstract String getUrl();

        public static Builder builder() {
            return new AutoValue_Project_User.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setUsername(String username);

            public abstract Builder setUrl(String url);

            public abstract User build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_Project_GitlabProject.Builder.class)
    public abstract static class GitlabProject {
        public abstract String getProjectGroup();

        public abstract List<String> getIgnoredSubGroups();

        public static Builder builder() {
            return new AutoValue_Project_GitlabProject.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setProjectGroup(String projectGroup);

            public abstract Builder setIgnoredSubGroups(List<String> ignoredSubGroups);

            public abstract GitlabProject build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_Project_GithubProject.Builder.class)
    public abstract static class GithubProject {
        public abstract String getOrg();

        public abstract List<String> getIgnoredRepos();

        public static Builder builder() {
            return new AutoValue_Project_GithubProject.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setOrg(String org);

            public abstract Builder setIgnoredRepos(List<String> ignoredRepos);

            public abstract GithubProject build();
        }
    }

    /**
     * Does not use autovalue as the value should be mutable.
     * 
     * @author Martin Lowe
     *
     */
    public static class Repo {
        private String url;

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
