/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.git.eca.api.models.CaptchaResponseData;

/**
 * Binding to validate hcaptcha validation requests.
 * 
 * @author Martin Lowe
 *
 */
@Produces(MediaType.APPLICATION_FORM_URLENCODED)
@RegisterRestClient(baseUri = "https://hcaptcha.com")
public interface HCaptchaCallbackAPI {

    @POST
    @Path("siteverify")
    CaptchaResponseData validateCaptchaRequest(@FormParam("response") String response, @FormParam("secret") String secret,
            @FormParam("sitekey") String sitekey);
}
