/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Information about the given Github ECA application installation
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GithubApplicationInstallation.Builder.class)
public abstract class GithubApplicationInstallation {

    public abstract int getId();

    public abstract String getTargetType();

    public abstract String getTargetId();

    public static Builder builder() {
        return new AutoValue_GithubApplicationInstallation.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(int id);

        public abstract Builder setTargetType(String targetType);

        public abstract Builder setTargetId(String targetId);

        public abstract GithubApplicationInstallation build();
    }
}
