/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author martin
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GithubCommitStatusRequest.Builder.class)
public abstract class GithubCommitStatusRequest {
    public abstract String getState();
    public abstract String getTargetUrl();
    public abstract String getDescription();
    public abstract String getContext();

    public static Builder builder() {
        return new AutoValue_GithubCommitStatusRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setState(String state);
        public abstract Builder setTargetUrl(String targetUrl);
        public abstract Builder setDescription(String description);
        public abstract Builder setContext(String context);

        public abstract GithubCommitStatusRequest build();
    }
}
