package org.eclipsefoundation.git.eca.api.models;

import java.util.List;

import org.eclipsefoundation.git.eca.api.models.Project.GitlabProject;
import org.eclipsefoundation.git.eca.api.models.Project.User;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_InterestGroupData.Builder.class)
public abstract class InterestGroupData {
    public abstract String getId();
    public abstract String getTitle();
    public abstract String getLogo();
    public abstract String getState();
    public abstract String getProjectId();
    public abstract Descriptor getDescription();
    public abstract Descriptor getScope();
    public abstract List<User> getLeads();
    public abstract List<User> getParticipants();
    public abstract GitlabProject getGitlab();

    public static Builder builder() {
        return new AutoValue_InterestGroupData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(String id);
        public abstract Builder setTitle(String title);
        public abstract Builder setGitlab(GitlabProject gitlab);
        public abstract Builder setLogo(String logo);
        public abstract Builder setState(String state);
        public abstract Builder setProjectId(String foundationdbProjectId);
        public abstract Builder setDescription(Descriptor description);
        public abstract Builder setScope(Descriptor scope);
        public abstract Builder setLeads(List<User> leads);
        public abstract Builder setParticipants(List<User> participants);
        public abstract InterestGroupData build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_InterestGroupData_Descriptor.Builder.class)
    public abstract static class Descriptor {
        public abstract String getSummary();
        public abstract String getFull();

        public static Builder builder() {
            return new AutoValue_InterestGroupData_Descriptor.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setSummary(String summary);
            public abstract Builder setFull(String full);
            public abstract Descriptor build();
        }
    }
}
