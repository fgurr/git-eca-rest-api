/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.jboss.resteasy.annotations.GZIP;

/**
 * Interface for interacting with the PMI Projects API. Used to link Git
 * repos/projects with an Eclipse project to validate committer access.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
@Path("/api")
@RegisterRestClient
@GZIP
public interface ProjectsAPI {

	/**
	 * Retrieves all projects with the given repo URL.
	 * 
	 * @param repoUrl the target repos URL
	 * @return a list of Eclipse Foundation projects.
	 */
	@GET
	@Path("projects")
	Response getProjects(@BeanParam BaseAPIParameters baseParams);

	@GET
	@Path("interest-groups")
	@Produces("application/json")
	Response getInterestGroups(@BeanParam BaseAPIParameters params);
}
