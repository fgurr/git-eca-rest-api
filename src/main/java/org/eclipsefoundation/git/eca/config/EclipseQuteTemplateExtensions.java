/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.config;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.dto.CommitValidationMessage;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;

import io.quarkus.qute.TemplateExtension;

@TemplateExtension
public class EclipseQuteTemplateExtensions {

    /**
     * Made to count nested errors in a list of commit status objects.
     * 
     * @param statuses the statuses to expand and count errors for
     * @return the number of errors in the list of statuses
     */
    static int getErrorCount(List<CommitValidationStatus> statuses) {
        return statuses.stream().mapToInt(s -> s.getErrors().size()).sum();
    }

    /**
     * Formats and flattens a list of statuses to a list of errors encountered while validation a set of commits.
     * 
     * @param statuses a list of commit validation statuses to retrieve errors from
     * @return a list of flattened errors, or an empty list if (none are found.
     */
    static List<CommitValidationMessage> getValidationErrors(List<CommitValidationStatus> statuses) {
        return statuses.stream().flatMap(s -> s.getErrors().stream()).collect(Collectors.toList());
    }

    /**
     * <p>
     * Obfuscates an email for public consumption, showing the first letter of the email address, and the domain of the
     * address for identification, and stripping out the rest of the address.
     * </p>
     * 
     * <p>
     * <strong>Example:</strong> <br />
     * Source: sample.address+123@eclipse.org <br />
     * Output: sample.address+123@ecl*pse DOT org
     * </p>
     * 
     * @param email the address to obfuscate
     * @return the obfuscated address, or empty string if (the string could not be parsed as an email address.
     */
    static String obfuscateEmail(String email) {
        if (StringUtils.isBlank(email)) {
            return "";
        }
        // split on the @ symbol
        String[] emailParts = email.split("\\@");
        if (emailParts.length != 2) {
            // valid emails will have 2 sections when split this way
            return "";
        }

        String[] domainParts = emailParts[1].split("\\.");
        if (domainParts.length < 2) {
            // emails should have at least 2 parts here
            return "";
        }
        // the index in the middle of the first domain part of the email address
        int middleIndexDomain = Math.floorDiv(domainParts[0].length(), 2);

        // build the obfuscated email address in the pattern defined in the block comment
        StringBuilder sb = new StringBuilder();
        sb.append(emailParts[0]);
        sb.append("@");
        sb.append(domainParts[0].substring(0, middleIndexDomain));
        sb.append('*');
        sb.append(domainParts[0].substring(middleIndexDomain + 1));
        for (int i = 1; i < domainParts.length; i++) {
            sb.append(" DOT ");
            sb.append(domainParts[i]);
        }
        return sb.toString();
    }

    /**
     * Standardized conversion of error codes into messages to be consumed downstream.
     * 
     * @param message the validation error to convert into a meaningful message
     * @return the message string for the given validation error.
     */
    static String getMessageForError(CommitValidationMessage message) {
        String out = "";
        if (message.getStatusCode() == -406) {
            out = String
                    .format("Committer does not have permission to push on behalf of another user (Legacy). (%s)",
                            EclipseQuteTemplateExtensions.obfuscateEmail(message.getCommitterEmail()));
        } else if (message.getStatusCode() == -405) {
            out = String
                    .format("Committer did not have a signed ECA on file. (%s)",
                            EclipseQuteTemplateExtensions.obfuscateEmail(message.getCommitterEmail()));
        } else if (message.getStatusCode() == -404) {
            out = String
                    .format("Author did not have a signed ECA on file. (%s)",
                            EclipseQuteTemplateExtensions.obfuscateEmail(message.getAuthorEmail()));
        } else if (message.getStatusCode() == -403) {
            out = String
                    .format("Committer does not have permission to commit on specification projects. (%s)",
                            EclipseQuteTemplateExtensions.obfuscateEmail(message.getCommitterEmail()));
        } else if (message.getStatusCode() == -402) {
            out = "Sign-off not detected in the commit message (Legacy).";
        } else if (message.getStatusCode() == -401) {
            out = "Request format/state error detected.";
        } else {
            out = "Unaccounted for error detected, please contact administrators.";
        }
        return out;
    }
}
