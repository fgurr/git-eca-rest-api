/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.dto;

import java.time.ZonedDateTime;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * Github Webhook request tracking info. Tracking is required to trigger revalidation through the Github API from this
 * service.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table
public class GithubWebhookTracking extends BareNode {
    public static final DtoTable TABLE = new DtoTable(GithubWebhookTracking.class, "gwt");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String installationId;
    private String repositoryFullName;
    private String headSha;
    private Integer pullRequestNumber;
    private String lastKnownState;
    private ZonedDateTime lastUpdated;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the installationId
     */
    public String getInstallationId() {
        return installationId;
    }

    /**
     * @param installationId the installationId to set
     */
    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    /**
     * @return the repositoryFullName
     */
    public String getRepositoryFullName() {
        return repositoryFullName;
    }

    /**
     * @param repositoryFullName the repositoryFullName to set
     */
    public void setRepositoryFullName(String repositoryFullName) {
        this.repositoryFullName = repositoryFullName;
    }

    /**
     * @return the headSha
     */
    public String getHeadSha() {
        return headSha;
    }

    /**
     * @param headSha the headSha to set
     */
    public void setHeadSha(String headSha) {
        this.headSha = headSha;
    }

    /**
     * @return the pullRequestNumber
     */
    public Integer getPullRequestNumber() {
        return pullRequestNumber;
    }

    /**
     * @param pullRequestNumber the pullRequestNumber to set
     */
    public void setPullRequestNumber(Integer pullRequestNumber) {
        this.pullRequestNumber = pullRequestNumber;
    }

    /**
     * @return the lastKnownState
     */
    public String getLastKnownState() {
        return lastKnownState;
    }

    /**
     * @param lastKnownState the lastKnownState to set
     */
    public void setLastKnownState(String lastKnownState) {
        this.lastKnownState = lastKnownState;
    }

    /**
     * @return the lastUpdated
     */
    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(headSha, id, installationId, lastKnownState, lastUpdated, pullRequestNumber, repositoryFullName);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GithubWebhookTracking other = (GithubWebhookTracking) obj;
        return Objects.equals(headSha, other.headSha) && Objects.equals(id, other.id)
                && Objects.equals(installationId, other.installationId) && Objects.equals(lastKnownState, other.lastKnownState)
                && Objects.equals(lastUpdated, other.lastUpdated) && Objects.equals(pullRequestNumber, other.pullRequestNumber)
                && Objects.equals(repositoryFullName, other.repositoryFullName);
    }

    @Singleton
    public static class GithubWebhookTrackingFilter implements DtoFilter<GithubWebhookTracking> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            String installationId = params.getFirst(GitEcaParameterNames.INSTALLATION_ID_RAW);
            if (StringUtils.isNotBlank(installationId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".installationId = ?",
                                new Object[] { installationId }));
            }
            String pullRequestNumber = params.getFirst(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW);
            if (StringUtils.isNumeric(pullRequestNumber)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".pullRequestNumber = ?",
                                new Object[] { Integer.parseInt(pullRequestNumber) }));
            }
            String repositoryFullName = params.getFirst(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW);
            if (StringUtils.isNotBlank(repositoryFullName)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".repositoryFullName = ?",
                                new Object[] { repositoryFullName }));
            }

            return statement;
        }

        @Override
        public Class<GithubWebhookTracking> getType() {
            return GithubWebhookTracking.class;
        }
    }
}
